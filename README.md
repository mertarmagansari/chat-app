## Chat App

A sample chat application project built with Socket.io and React JS.

Current features of this application:
* Users can type their names to use as their usernames
* Users can do realtime chat with other users
* Users can see all messages from other users at the same time

What will be implemented?
* Users should be able to see older messages that are written before they login to chat (history of chat)
* The front-end and back-end sides of the application may be seperated and defined well

To run this application:
* Clone this repository into a local directory
* Change directory to the repository's directory (chat-app)
* Run `npm install` to install dependencies
* Run `npm start` to run the project

The application should start on port `3000` by default.

To stop this application:
* Press `Control + C` while in the terminal

Note: The code is referred to this repository;
https://github.com/socketio/socket.io/tree/master/examples/chat